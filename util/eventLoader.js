/*
author: Jelmer Oldersma
version: 1.0
*/

const reqEvent = (event) => require(`../events/${event}`);
//Executes the proper eventhandler for the given event
module.exports = client => {
  client.on('ready', () => reqEvent('ready')(client)); //When client is ready to work
  client.on('reconnecting', () => reqEvent('reconnecting')(client)); //triggered when internet connection is back
  client.on('disconnect', () => reqEvent('disconnect')(client)); //triggered when internet connection is lost, but bot is still working
  client.on('message', message => reqEvent('message')(message, client)); //For each message send (where the bot has access)
  client.on('guildCreate', guild => reqEvent('guildCreate')(guild, client));//Triggered when added to new guild
  client.on('guildDelete', guild => reqEvent('guildDelete')(guild, client));//Triggered when kicked from a guild
  client.on('error', err => reqEvent('error')(err, client));
  client.on('messageDelete', message => reqEvent('messageDelete')(message, client));
};
