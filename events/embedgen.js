const Discord = require('discord.js');
const admin = require('firebase-admin');
const db = admin.database();

module.exports = embedgen;

async function checkArray(array) { //Returns 'none' string if array has no players, or creates string list with the players
  if(array.length == 0) { //If array has no players
    return 'none';
  } else { //Add each player to string, plus newline, creating a list
    array = array.join('\n');
    return array;
  }
}

async function embedgen(players, bot, guild) { //Creates embed with the results
  let acceptsP = {}; //object going to hold all players who accepted
  Object.keys(players).forEach(function(key) { //looping through all players
    if(players[key].answer === 'true') { //if they have accepted, they get added to local acceptsP object
      acceptsP[key] = players[key];
    }
  });

  //Player lists, Tank Healer Dps, Charactername + discordusername
  let tc = [];  let tn = [];
  let hc = [];  let hn = [];
  let dc = [];  let dn = [];
  let tanks = []; let healers = []; let dps = []; //invisible arrays holding players id (users) for raidteam command

  //Checks the players character for their spec, and adds them to the correct arrays
  Object.keys(acceptsP).forEach(function(key) {
    let usr = {
      username: acceptsP[key].user,
      id: acceptsP[key].id
    };
    if(bot.localdb.guilds[guild.id].members.hasOwnProperty(usr.id)){ //Check if user has a character added
      switch(guild.members[usr.id].characters.main.role) { //Check what spec character is
        case 'TANK': //If character spec is tank
          tc.push(acceptsP[key].char); //Tank Character name array
          tn.push(usr.username); //Tank discord username array
          tanks.push(usr.id); //hidden tank user id array
          break;
        case 'HEALING': //If character is healer
          hn.push(usr.username); //Same as tank
          hc.push(acceptsP[key].char);
          healers.push(usr.id);
          break;
        case 'DPS': //If character is dps
          dn.push(usr.username); //Same as tank
          dc.push(acceptsP[key].char);
          dps.push(usr.id);
          break;
      }
    }
  });

  var embed = new Discord.RichEmbed()
    .setTitle('Raid Event')
    .setColor(0x00AE86)
    .setTimestamp()
    .addField('Tanks:', await checkArray(tc), true) //add tank arrays content into embed
    .addField('Name:', await checkArray(tn), true)
    .addBlankField()
    .addField('Healers:', await checkArray(hc), true) //add healer arrays content into embed
    .addField('Name:', await checkArray(hn), true)
    .addBlankField()
    .addField('Dps:', await checkArray(dc), true) //add dps arrays content into embed
    .addField('Name:', await checkArray(dn), true);


  let obj = {tanks: tanks, healers: healers, dps: dps}; //Create object with raidteam as content

  let ref = db.ref('/guilds/'+guild.id+'/raid/team/');
  ref.update(obj); //update firebase with the object
  return await embed; //returns the results embed
}
