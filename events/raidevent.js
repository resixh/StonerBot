/*
author: Jelmer Oldersma
version: 0.1
*/
const reactsEvent = require('./reactions'); //reactionsListener
const embedgen = require('./embedgen.js');
const embedupdate = require('./embedupdate.js');
const admin = require('firebase-admin');
const messageDelete = require('./messageDelete.js');
const db = admin.database();

module.exports = raidevent;

async function raidevent(bot, sentMessage, guild, author, random, output, embed, restart) {
  sentMessage.reacts = {players: {}};
  if(restart) {
    sentMessage.reacts.players = restart;
    let r = await sentMessage.reactions.find(re => re.emoji.name === '✅');
    let users = await r.fetchUsers();

    users.forEach(user => {
      if(!user.bot) {
        let character = '-';

        if(!bot.localdb.guilds[guild.id].members.hasOwnProperty(user.id)) { //Check if user has a character linked.
          //Send user that he hasn't added one, and how to do so
          user.send(user.username+', you dont have a character added to your account. Please do that first: `'+guild.prefix+'recruit [charactername] [realm]` (Without the brackets`[ ]` ofcourse). If you don\'t before the event closes, you\'ll be added to declined list.');
        } else {
          character = guild.members[user.id].characters.main.name + ' (' + guild.members[user.id].characters.main.spec + '/' + guild.members[user.id].characters.main.class + ')';
        }

        sentMessage.reacts.players[user.id] = {
          answer: 'true',
          char: character,
          id: user.id,
          user: user.username
        };
        r.remove(user);
      }
    });
  }
  reactsEvent(bot, sentMessage, guild, author, random); //Starts listener

  bot.on(random, (r, user) => { //Triggers when an user reacts to the message
    reactfn(r, sentMessage, user, output, guild, bot, embed);//Reaction handler
  });
}

//Reaction Handler
async function reactfn(emoji, msg, usr, output, guild, bot, embed) {
  let icon = emoji._emoji.name; //Get the actual emoji used
  let players = msg.reacts.players; //Get the message local reactions object
  if(!bot.localdb.guilds[guild.id].members.hasOwnProperty(usr.id)) { //Check if user has a character linked.
    //Send user that he hasn't added one, and how to do so
    usr.send(usr.username+', you dont have a character added to your account. Please do that first: `'+guild.prefix+'recruit [charactername] [realm]` (Without the brackets`[ ]` ofcourse). If you don\'t before the event closes, you\'ll be added to declined list.');
  }

  switch(icon) { //Tries to match used emoji with one in the switch
    case '💀': //Skull also stops listener
      messageDelete(msg, bot);//Sends results to setup'd channel
      let content = 'This event invitation has been closed!';//Edit message to make this the title
      msg.edit(content);
      db.ref('/guilds/'+guild.id+'/raid/msg/'+msg.id).remove();
      return;
    case '👽': //Get results without closing the invitation
      bot.channels.get(guild.modChannel).send(await embedgen(players, bot, guild));
      break;
    case '✅': //When user accepts
      players[usr.id] = { //Add user to message's local object
        answer: 'true',
        user: usr.username, //add user object for future reference
        id: usr.id
      };
      if(bot.localdb.guilds[guild.id].members.hasOwnProperty(usr.id)){ //gets user's setup'd character name
        players[usr.id].char = guild.members[usr.id].characters.main.name + ' (' + guild.members[usr.id].characters.main.spec + '/' + guild.members[usr.id].characters.main.class + ')';
      } else { //if user doesnt have one:
        players[usr.id].char = '-';
      }
      break;
    case '❌': //When user declines the invite
      players[usr.id] = { //Add user to msg local object
        answer: 'false',
        user: usr.username,
        id: usr.id
      };
      if(bot.localdb.guilds[guild.id].members.hasOwnProperty(usr.id)){ //gets user character's name
        players[usr.id].char = guild.members[usr.id].characters.main.name + ' (' + guild.members[usr.id].characters.main.spec + '/' + guild.members[usr.id].characters.main.class + ')';
      } else { //if user doesnt have one:
        players[usr.id].char = '-';
      }
      break;
    case '❓': //Unsure users
      players[usr.id] = {
        answer: 'unsure',
        user: usr.username,
        id: usr.id
      };
      if(bot.localdb.guilds[guild.id].members.hasOwnProperty(usr.id)){
        players[usr.id].char = guild.members[usr.id].characters.main.name + ' (' + guild.members[usr.id].characters.main.spec + '/' + guild.members[usr.id].characters.main.class + ')';
      } else {
        players[usr.id].char = '-';
      }
      break;
    default:
      break;
  }
  let newEmbed = await embedupdate(bot, players, embed, guild);
  await msg.edit(newEmbed.msg);

  db.ref('/guilds/'+guild.id+'/raid/msg/'+msg.id).update({
    msgContent: newEmbed.db,
    players: players
  });
}
