const Discord = require('discord.js');

module.exports = embedupdate;

async function embedupdate(bot, players, oldEmbed, guild) {
  let ofields = oldEmbed.fields;
  var embed = new Discord.RichEmbed() //Embed send to the raiding channel
    .setTitle('Raid Event')
    .setColor(0x00AE86)
    .setTimestamp()
    .addField('What:', ofields[0].value) //subject
    .addField('Accepted:', 'none', true) //Users who accept get into here
    .addField('Character:', '-', true)
    .addBlankField()
    .addField('Declined:', 'none', true) //Who decline
    .addField('Character:', '-', true)
    .addBlankField()
    .addField('Not sure yet:', 'none', true) //Who are unsure if they can attend
    .addField('Character:', '-', true)
    .addBlankField()
    .setFooter('Tanks: 0, Healers: 0, Melee: 0, Ranged: 0');
  let data = {};
  let accepted = [];  let ac2 = []; //Accepted arrays: User name, character name
  let declined = [];  let dec2 = []; //Declined arrays: User name, character Name
  let unsure = [];  let uns2 = []; //Unsure arrays: .. ..
  let roleNum = {
    tanks: 0,
    healers: 0,
    ranged: 0,
    melee: 0
  };

  Object.keys(players).forEach(function(key) { //Pushes players in msg local object into the right arrays
    if(players[key].answer === 'true') { //When accepted
      accepted.push(players[key].user);
      ac2.push(players[key].char);
      if(bot.localdb.guilds[guild.id].members.hasOwnProperty(key)) {
        switch(bot.localdb.guilds[guild.id].members[key].characters.main.spec) {
          //WARRIOR
          case 'Arms':
            roleNum.melee++;
            break;
          case 'Fury':
            roleNum.melee++;
            break;
          //WARRIOR+PALA
          case 'Protection':
            roleNum.tanks++;
            break;
          //PALA
          case 'Retribution':
            roleNum.melee++;
            break;
          //PALA+PRIEST
          case 'Holy':
            roleNum.healers++;
            break;
          //PRIEST
          case 'Shadow':
            roleNum.ranged++;
            break;
          case 'Discipline':
            roleNum.healers++;
            break;
          //HUNTER
          case 'Beast Mastery':
            roleNum.ranged++;
            break;
          case 'Marksmanship':
            roleNum.ranged++;
            break;
          case 'Survival':
            roleNum.melee++;
            break;
          //Rogue
          case 'Assassination':
            roleNum.melee++;
            break;
          case 'Outlaw':
            roleNum.melee++;
            break;
          case 'Subtlety':
            roleNum.melee++;
            break;
          //DEATH KNIGHT
          case 'Unholy':
            roleNum.melee++;
            break;
          case 'Blood':
            roleNum.tanks++;
            break;
          //DEATH KNIGHT+MAGE
          case 'Frost':
            if(bot.localdb.guilds[guild.id].members[key].characters.main.class == 'Death Knight') {
              roleNum.melee++;
            }
            else if (bot.localdb.guilds[guild.id].members[key].characters.main.class == 'Mage') {
              roleNum.ranged++;
            }
            break;
          //MAGE
          case 'Arcane':
            roleNum.ranged++;
            break;
          case 'Fire':
            roleNum.ranged++;
            break;
          //SHAMAN
          case 'Elemental':
            roleNum.ranged++;
            break;
          case 'Enhancement':
            roleNum.melee++;
            break;
          //SHAMAN+DRUID
          case 'Restoration':
            roleNum.healers++;
            break;
          //DRUID
          case 'Feral':
            roleNum.melee++;
            break;
          case 'Balance':
            roleNum.ranged++;
            break;
          case 'Guardian':
            roleNum.tanks++;
            break;
          //WARLOCK
          case 'Affliction':
            roleNum.ranged++;
            break;
          case 'Demonology':
            roleNum.ranged++;
            break;
          case 'Destruction':
            roleNum.ranged++;
            break;
          //MONK
          case 'Brewmaster':
            roleNum.tanks++;
            break;
          case 'Windwalker':
            roleNum.melee++;
            break;
          case 'Mistweaver':
            roleNum.healers++;
            break;
          //DEMON Hunter
          case 'Havoc':
            roleNum.melee++;
            break;
          case 'Vengeance':
            roleNum.tanks++;
            break;
        }
      }
    } else if (players[key].answer === 'false') { //When declined
      declined.push(players[key].user);
      dec2.push(players[key].char);
    } else { //When unsure
      unsure.push(players[key].user);
      uns2.push(players[key].char);
    }
  });

  //Update current embed with new player lists (the arrays)
  embed.fields[1].value = await checkArray(accepted);
  embed.fields[2].value = await checkArray(ac2);
  embed.fields[4].value = await checkArray(declined);
  embed.fields[5].value = await checkArray(dec2);
  embed.fields[7].value = await checkArray(unsure);
  embed.fields[8].value = await checkArray(uns2);
  embed.setFooter('Tanks: '+roleNum.tanks+', Healers: '+roleNum.healers+', Melee: '+roleNum.melee+', Ranged: '+roleNum.ranged);

  data.msg = embed;

  const removeEmpty = async (obj) => {
    Object.keys(obj).forEach(key => {
      if (obj[key] && typeof obj[key] === 'object') removeEmpty(obj[key]);
      else if (obj[key] === undefined) delete obj[key];
    });
    return obj;
  };

  data.db = await removeEmpty(embed);
  return data;
}

async function checkArray(array) { //Returns 'none' string if array has no players, or creates string list with the players
  if(array.length == 0) { //If array has no players
    return 'none';
  } else { //Add each player to string, plus newline, creating a list
    array = array.join('\n');
    return array;
  }
}
