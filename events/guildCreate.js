/*
author: Jelmer Oldersma
version: 1.0
*/
const settings = require('../settings.json'); //default settings
const admin = require('firebase-admin');
const db = admin.database();
//When the bot gets added to a new guild
module.exports = async (guild, client) => {

  //Setup basic guild info
  let obj = {
    id: guild.id,
    name: guild.name,
    ownerid: guild.owner.id,
    prefix: settings.prefix,
    setup: false
  };
  //
  //Check databases existence and add guild to local and firebase db
  if(client.localdb.guilds === null){ //This should never be possible
    console.log('localdb is empty!');
    client.localdb.guilds = {};
    client.localdb.guilds[guild.id] = obj;
  } else { //Update localDatabase with new settings
    console.log('localdb is not empty!');
    client.localdb.guilds[guild.id] = obj;
  }
  await db.ref('/guilds/' + guild.id).set(obj) //Update firebase database, then send message with first-time info
      .then(() => sendDefaultInfo(guild))
    .catch(console.error);
  console.log('Joined ' + guild.name);
};

//Searches for the first writable channel
async function sendDefaultInfo(guild) {
  guild.channels.forEach(channel => {
    //Check if we can write here
    if (channel.type === 'text' && channel.permissionsFor(guild.me).has('SEND_MESSAGES')) {
      //Found writable channel
      channel.send(`Hi there! My prefix is \`${settings.prefix}\`. To use all my functions please use the \`${settings.prefix}setup\` command.`);
    }
  });
}

//