/*
author: Jelmer Oldersma
version: 1.0
*/

const admin = require('firebase-admin');
const db = admin.database();
//When bot gets kicked from guild.
module.exports = async (guild, client) => {
  //Delete the guild from local and firebase db
  delete client.localdb.guilds[guild.id];
  await db.ref('/guilds/' + guild.id).remove()
    .then(console.log(`Left ${guild.name}`))
    .catch(console.error);
};
