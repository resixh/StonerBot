/*
author: Jelmer Oldersma
version: 1.9
*/
const moment = require('moment');
const log = message => {
  console.log(`[${moment().format('YYYY-MM-DD HH:mm:ss')}] ${message}`);
};
//const raidevent = require('../events/raidevent');
const admin = require('firebase-admin');
const db = admin.database();

module.exports = async client => { //When startup has been completed
  log('IM READY');
  await client.user.setActivity('Mention me for your prefix!', {type: 'PLAYING'});

  await startupCalendar(client);
};

async function startupCalendar(client) {
  await client.updateDB().then(() => {
    let guilds = client.localdb.guilds;
    if (guilds != null) {
      Object.keys(guilds).forEach((a) => {
        console.log(a);
        if (client.guilds.get(guilds[a].id)) {
          console.log('bot sees this guild!');
          // Do logic with guild

        }
      });
    }
  });
}

// if (guilds[a].raid && guilds[a].raid.msg) {
//   let ref = guilds[a].raid.msg;
//   Object.keys(ref).forEach((b) => {
//     let guild = client.guilds.get(guilds[a].id);
//     let channel = guild.channels.get(guilds[a].raidChannel);
//     let author = ref[b].author;
//     let random = ref[b].random;
//     let output = guilds[a].modChannel;
//     let embed = ref[b].msgContent;
//     let restart = ref[b].players;
//     channel.fetchMessage(b)
//         .then(async sentMessage => {
//           let guild = guilds[a];
//           await raidevent(client, sentMessage, guild, author, random, output, embed, restart);
//         })
//         .catch(async err => {
//           //console.error(err);
//           await db.ref('/guilds/' + guild.id + '/raid/msg/' + b).remove();
//         });
//   });
// }