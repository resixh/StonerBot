const moment = require('moment');
const log = message => {
  console.log(`[${moment().format('YYYY-MM-DD HH:mm:ss')}] ${message}`);
};

module.exports = async (err, client) => { //When startup has been completed

  log(err);

};
