/*
author: Jelmer Oldersma
version: 1.0
*/

const moment = require('moment');
const log = message => {
  console.log(`[${moment().format('YYYY-MM-DD HH:mm:ss')}] ${message}`); //Replaces default console log with date/time added one
};
const settings = require('../settings.json');
module.exports = (message, client) => {
  if (message.author.bot) return; //returns if message came from a bot
  let prefix;
  let guildid;
  let guild;
  //check if message is send in dm or guildchannel
  guild = settings;
  prefix = settings.prefix;
  if((message.guild != null) && client.localdb.hasOwnProperty('guilds')) { //Check if channel is guild, and guild is known
    guildid = message.guild.id;
    if(client.localdb.guilds.hasOwnProperty(guildid)) { //gets the guild settings
      prefix = client.localdb.guilds[message.guild.id].prefix;
      guild = client.localdb.guilds[message.guild.id];
    }
  }
  //If bot gets mentioned directly, reply with the guilds prefix or the default one (depending on channel used)
  if(message.isMemberMentioned(client.user) && !message.content.includes('@everyone') && !message.content.includes('@here')){
    message.reply('My prefix is `' + guild.prefix + '`.\nType `'+ guild.prefix +'help` for all available commands.');
    return;
  }


  //check if message was send with the set prefix
  if (!message.content.startsWith(prefix)) return;
  let command = message.content.split(' ')[0].slice(prefix.length); //Get command
  let params = message.content.split(' ').slice(1); //Get extra text given
  let perms = client.elevation(message, guild); //gets user permlvl
  let cmd =  (client.commands.has(command))? //check if command exists
    client.commands.get(command):
    ((client.aliases.has(command))?
      client.commands.get(client.aliases.get(command)):
      null);

  if (cmd!=null) { //Executes command
    log('CommandLog: ' + command + ' ' + message.author.username); //logs which command, and which user used it
    if (perms < cmd.conf.permLevel) { //check if user has high enough permlvl
      message.author.send('You can\'t use that command. It requires permLevel '+cmd.conf.permLevel+' and you have '+ perms+'.\nCommands sent in pm\'s are always permLevel 0.');
      return;
    } //Run the actual command, sending along all needed data
    if(perms === 1 && guild.hasOwnProperty("command_roles") && guild.command_roles.hasOwnProperty(cmd.help.name)) {
      let bool = false;
      let guild_roles = guild.command_roles[cmd.help.name];
      guild_roles.forEach(role_id => {
        if(message.member.roles.has(role_id)) {
          bool = true
        }
      });
      if(!bool) {
        return;
      }
    }
    cmd.run(client, message, guild, params, perms, command);
  }

};