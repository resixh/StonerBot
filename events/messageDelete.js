const admin = require('firebase-admin');
const db = admin.database();
const embedgen = require('./embedgen.js');

module.exports = async (msg, client) => {
  return;
  try {
    if (client.localdb.guilds[message.guild.id].raid.msg.hasOwnProperty(message.id)) {
      let message = msg;
      let guild = client.localdb.guilds[message.guild.id];
      let players = message.reacts.players;

      const ref = db.ref('/guilds/' + guild.id + '/raid/');

      if (guild.raid.msg.hasOwnProperty(message.id)) {
        let counterInt = 0;
        if (guild.raid.hasOwnProperty('counter')) {
          counterInt = guild.raid.counter + 1;
        } else {
          counterInt++
        }
        await ref.child('counter').set(counterInt);
        client.localdb.guilds[guild.id].raid['counter'] = counterInt;

        for (let playerId in players) {
          if (players.hasOwnProperty(playerId)) {
            let player = players[playerId];
            let playerCounter = 0;
            if (guild.members.hasOwnProperty(player.id) && guild.members[player.id].hasOwnProperty('counter')) {
              playerCounter = guild.members[player.id].counter + 1;
            } else {
              playerCounter++;
            }
            await db.ref('/guilds/' + guild.id + '/members/' + player.id + '/counter').set(playerCounter);
            client.localdb.guilds[guild.id].members[player.id].counter = playerCounter;
          }
        }

        client.channels.get(guild.modChannel).send(await embedgen(players, client, guild));
        await ref.child(message.guild.id + '/raid/msg/' + message.id).remove();
      }
    }
  } catch(err) {console.error(err)}
};