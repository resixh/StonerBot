/*
author: Jelmer Oldersma
version: 1.0
*/

const moment = require('moment');
const log = message => {
  console.log(`[${moment().format('YYYY-MM-DD HH:mm:ss')}] ${message}`);
};
module.exports = client => {
  log('Im reconnecting <3'); //When bot reconnects to server
};
