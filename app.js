#!/usr/bin/env node

/*
author: Jelmer Oldersma

1. Code optimisation
*/
const Version = '2.1.6';
const Discord = require('discord.js');
let client = new Discord.Client(); //The main class (the client where everything runs on)
const settings = require('./settings.json'); //Main config, used as default if personalised settings aren't available.
const fs = require('fs');
const moment = require('moment');
require('./util/eventLoader')(client);
const log = message => {
  console.log(`[${moment().format('YYYY-MM-DD HH:mm:ss')}] ${message}`);
};

//Firebase config
const admin = require('firebase-admin');
const serviceAccount = require('./stonerbot.json');
//FIREBASE CONNECTION
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://stonerbot-194617.firebaseio.com'
});
//

//create localdb
client.localdb = {
  version: Version,
  guilds: {}
};

//copy firebase db to the localdb to reduce queries to firebase (copies only the guild tree)
client.updateDB = async function () {
  let db = admin.database().ref('/guilds/');
  await db.once('value', data => {
    client.localdb.guilds = data.val();
  });
  log('✅Local DB updated!✅');
};
//

//Music queues
client.queue = new Map();
//

//Loads all commands in commands folder
client.commands = new Discord.Collection();
client.aliases = new Discord.Collection();

function addCommand(filepath) {
  let props = require(`./commands/${filepath}`);
  log(`Loading Command: ${props.help.name}. 👌`);
  client.commands.set(props.help.name, props);
  props.conf.aliases.forEach(alias => {
    client.aliases.set(alias, props.help.name);
  });
}

//reads all files
fs.readdir('./commands/', {withFileTypes: true}, (err, files) => {
  if (err) console.error(err);
  //Returned files obj is a fs.dirent[]
  files.forEach(f => {
    if(f.isFile()){
      addCommand(f.name);
    } else {
      fs.readdir(`./commands/${f.name}/`, (err, moduleFiles) => {
        log(`--- Module: ${f.name} ---`);
        moduleFiles.forEach(commandFile => {
          addCommand(`${f.name}/${commandFile}`);
        });
      })
    }
  })
});
//

//Slightly better than using the native awaitMessages
client.awaitMessage = async function(filter, channel, data) {
  await channel.awaitMessages(filter, {maxMatches: 1, time: 180000, errors: ['time']}) //Await user reply
      .then(collected => {
        data.push(collected.first().content); //Push reply content into data array
      })
      .catch(console.error);
};
//

//Permissions check
client.elevation = (message, _guild) => {
  /* This function should resolve to an ELEVATION level which
     is then sent to the command handler for verification*/
  let permlvl = 0;
  let guildAvailable;
  guildAvailable = message.guild !== null;
  if(guildAvailable){ //if not in guild, permlvl stays 0, else here we check what guildlevel user is
    permlvl = 1; //guild default lvl
    let mod_role = message.guild.roles.find(val => val.name === _guild.modrole); //Check if user has mod role
    if (mod_role && message.member.roles.has(mod_role.id)) permlvl = 2; //If yes, set to 2
    let admin_role = message.guild.ownerID; //Check if user is guild owner
    if (admin_role === message.author.id) permlvl = 3; //If yes, set to 3
    if (settings.owner.includes(message.author.id)) permlvl = 4; //When I sent a message in a guild its always 4 :D
  }
  return permlvl; //returns the resolved permlvl
};
//

//Connection to discord
client.login(settings.token);
