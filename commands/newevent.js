/*
author: Jelmer Oldersma
version: 0.9


1. Code optimalisation
2. Comments
*/


const admin = require('firebase-admin');
const Discord = require('discord.js');
const db = admin.database();
const filter = m => {if(!m.author.bot){return true;}else{return false;}};
const reactsEvent = require('../events/reactions');
var nanoid = require('nanoid');
var data = [];

exports.run = async (client, message, guild, params) => {
  let user = message.author;
  let pm = await user.createDM().catch(console.error);
  let ref = db.ref('/guilds/'+ guild.id);
  let output;
  let random = nanoid(); //generates random string (secure)

  //console.log(params);
  //console.log(ref);
  //Starts the private chat with user
  {
    pm.send('Hello there! What are we going to do? [event name]');
    await am(filter, pm);
    pm.send('And when? [your prefered date format]');
    await am(filter, pm);
    pm.send('Any description? (optional -> reply with `none`)');
    await am(filter, pm);
  }
  if(params) {
    output = params[0];
  } else {
    pm.send('What\'s the channel id where you want the message? (Use \\#`yourchannelname` in your guildchannel, to get the id.)' );
    await am(filter, pm);
    output = data[data.length - 1];
  }
  console.log(output);
  output = output.replace(/\D/g,'');
  output = message.guild.channels.get(output);






  message.channel.send(emGen(user)).then(async sentMessage => {
    //Starts eventEmitter on message reactions
    reactsEvent(client, sentMessage, guild, user, random);
    sentMessage.reacts = {};
    //The eventlistener, waiting for reactions
    client.on(random, (r, user) => {
      reactfn(r, sentMessage, user, output);
    });
  }).catch(console.error);
};

//What to do with each reaction/emoji
async function reactfn(emoji, msg, usr, output) {
  let icon = emoji._emoji.name;
  let reactions = msg.reacts; //the vote counter
  if(reactions.voters.hasOwnProperty(usr.id) && icon != '💀' && icon != '👽'){
    console.log('User Already voted!');
    return;
  }

  switch(icon) {
    case '💀':
      msg.send('BAM!');
      output.send('KLABAM!');
      break;
    default:
      console.log(icon);
  }
}

//Slightly better than using the native awaitMessages
async function am(filter, pm) {
  await pm.awaitMessages(filter, {maxMatches:1,time:180000,errors:['time']})
    .then(collected => {
      data.push(collected.first().content);
    })
    .catch(console.error);
}
//

//Embed gen
function emGen(user) {
  const embed = new Discord.RichEmbed()
    .setTitle('Event Invitation:')
    .setAuthor(user.tag, user.avatarURL)
    .setColor(0x00A786)
    .setTimestamp()
    .addField('When', data[1])
    .addField(data[0], data[2]);

  return embed;
}

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: ['ne'],
  permLevel: 2
};

exports.help = {
  name: 'newevent',
  description: 'Create a new event.',
  usage: 'newevent'
};
