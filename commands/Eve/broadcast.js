/*
author: Jelmer Oldersma
version: 0.1
*/

let data = [];

exports.run = async (client, message, guild, params, perms, command) => {
    let msgString = 'Ping:\n';
    if (!params) {
        data = [];
        let user = message.author;
        let msgchannel = message.channel;
        const filter = match => {
            return !match.author.bot && match.author === user;
        };

        let questionArray = [
            'What do you want to broadcast Sir',
            'FleetCommander name',
            'Fleetname',
            'Formup location',
            'Reimbursement type',
            'Comms',
            'Doctrine'
        ];

        let qmsg; //question msg var, to delete previous question
        for (const q of questionArray) {
            msgchannel.send(q + '?')
                .then(m => {
                    qmsg = m;
                });
            await client.awaitMessage(filter, msgchannel, data);
            if (qmsg) {
                qmsg.delete();
            }
        }

        //create msg

        msgString = msgString + data[0];

        for (let i = 1; i < questionArray.length; i++) {
            msgString = msgString + '\n' + questionArray[i] + ': ' + data[i];
        }
    } else {
        let subNum = guild.prefix.length + command.length;
        let content = message.content.substr(subNum);
        msgString = msgString + content;
    }

    //send check to HighCommand channel
    if(!guild.commandChannel) {
        return message.reply('Oi! Set the command channel first mate!');
    }

    let commandReaction = true;
    let commandChannel = client.channels.get(guild.commandChannel);
    await commandChannel.send(msgString)
        .then(m => {
            m.react('✅');
            m.react('❌');
            const rFilter = (reaction, user) => {
                return !user.bot;
            };
            const collector = m.createReactionCollector(rFilter, {time: 600000});
            collector.on('collect', async reaction => {
                if(reaction._emoji.name === '✅') {
                    await collector.stop('Accepted');

                } else if(reaction._emoji.name === '❌') {
                    await collector.stop('Denied');
                    return m.delete();
                }
            });
            collector.on('end', async (collected, reason) => {
                switch(reason) {
                    case "time":
                        if (!guild.pingChannel) {
                            await message.reply('Please set the ping channel mate :D');
                            return m.delete();
                        } else {
                            await client.channels.get(guild.pingChannel).send(msgString);
                            return m.delete();
                        }
                    case "Accepted":
                        if (!guild.pingChannel) {
                            await message.reply('Please set the ping channel mate :D');
                            return m.delete();
                        } else {
                            await client.channels.get(guild.pingChannel).send(msgString);
                            return m.delete();
                        }
                    case "Denied":
                        let r = await m.reactions.find(re => re.emoji.name === '❌');
                        let user = await r.fetchUsers().then(async users => {
                            return users.find(user => !user.bot);
                        });
                        await message.author.send(`Oh noes, ${user.tag} declined your request :O`);
                        break;
                }
            });
        });
};

exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: ['requestbroadcast', 'broadcast'],
    permLevel: 1
};

exports.help = {
    name: 'broadcast',
    description: 'Broadcast your message',
    usage: 'broadcast'
};
