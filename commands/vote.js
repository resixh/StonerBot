/*
author: Jelmer Oldersma
version: 0.9


1. Code optimalisation
2. Comments
*/

var nanoid = require('nanoid');
const reactsEvent = require('../events/reactions');

exports.run = async (client, message, guild, params, perms) => {
  let content = message.content.slice(guild.prefix.length).slice(5); //Gets the message's content without the command prefix
  let author = message.author;
  let random = nanoid(); //generates random string (secure)
  let param = params[params.length - 1];
  content = content.substr(0, content.length - param.length);
  //Check which parameter is used (including failsafe dm)
  //And sends the named results message to the wanted channel
  let output;
  switch(param) {
    case 'pm': //Sends msg to the author of the vote
      output = author;
      break;
    case 'here': //Sends msg to the channel where the voting was held
      output = message.channel;
      break;
    case 'mod': //Sends msg to the mod channel
      var pm = await author.createDM().catch(console.error);
      if(guild.setup == 'false') {
        pm.send('Your guild hasnt used the setup yet! Please use that first, then we can discuss this voting results channel into `mod` again.');
      } else {
        if(perms >= guild.roles.mod.lvl) {
          output = client.channels.get(guild.modlog).catch(error => pm.send(error));
        }

      }
      break;
    default:
      author.send(param + ' is not an option! `pm` = names only sent to you, `here` = names in the used channel. `mod` = names in mod channel.');
      message.delete();
      return;
  }
  //Check if bot can edit the message
  if(message.editable) {
    console.log('I can edit it! That shouldnt be possible...');
    return;
  } else if(message.deletable) {
    //The main script. Deletes user message, and send bot's version of it, with reaction listener attached
    console.log('I can delete it!');
    let messagechannel = message.channel;
    message.delete();
    // Here gets the message send and reaction listener added!
    messagechannel.send(content).then(async sentMessage => {
      //Starts eventEmitter on message reactions
      reactsEvent(client, sentMessage, guild, author, random);
      sentMessage.reacts = {
        yes: 0,
        no: 0,
        unsure: 0,
        yUsers: {},
        nUsers: {},
        unUsers: {},
        voters: {}
      };
      //The eventlistener, waiting for reactions
      client.on(random, (r, user) => {
        reactfn(r, sentMessage, user, output);
      });
    }).catch(console.error);
    //
  } else {
    //Bot cant delete original message.
    console.log('Cant do anything :(');
    message.channel.send('Cant do anything mate! Fix my roles...');
  }
};
//Function to handle the given reactions
async function reactfn(emoji, msg, usr, output) {
  let icon = emoji._emoji.name;
  let reactions = msg.reacts; //the vote counter
  if(reactions.voters.hasOwnProperty(usr.id) && icon != '💀' && icon != '👽'){
    console.log('User Already voted!');
    return;
  }
  //Check what to do with each reaction
  var y = splitUsers(reactions.yUsers);
  var n = splitUsers(reactions.nUsers);
  var un = splitUsers(reactions.unUsers);
  switch(icon) {
    case '💀':
      await msg.channel.fetchMessage(msg.id).then(prom => prom.delete().then(msg => {
        msg.channel.send(msg.content + '\n\nResults:\n' + reactions.yes + 'x accepted,\n' + reactions.no + 'x declined,\n' + reactions.unsure + 'x didn\'t know for sure.');
        output.send(`Results:\nYes voters:\n${y}\n\nNo voters:\n${n}\n\nUnsure voters:\n${un}`);
      }));
      break;
    case '👽':
      //msg.channel.send('Results:\n' + reactions.yes + 'x accepted,\n' + reactions.no + 'x declined,\n' + reactions.unsure + 'x didn\'t know for sure.\nListener ended.');
      output.send(`Results:\nYes voters:\n${y}\n\nNo voters:\n${n}\n\nUnsure voters:\n${un}`);
      break;
    case '✅':
      reactions.yes++;
      reactions.yUsers[usr.id] = usr;
      reactions.voters[usr.id] = usr;
      break;
    case '❌':
      reactions.no++;
      reactions.nUsers[usr.id] = usr;
      reactions.voters[usr.id] = usr;
      break;
    case '❓':
      reactions.unsure++;
      reactions.unUsers[usr.id] = usr;
      reactions.voters[usr.id] = usr;
      break;
    default:
      break;
  }
}

function splitUsers(userList) {
  if(Object.keys(userList).length === 0 && userList.constructor === Object) {
    return '';
  }
  let string = '';
  for(var child in userList) {
    let miniString = '`' + userList[child].tag + '` ';
    string = string.concat(miniString);
  }
  return string;
}

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: ['poll', 'Vote', 'Poll'],
  permLevel: 0
};

exports.help = {
  name: 'vote',
  description: 'Creates a message with voting options.',
  usage: 'vote [your message] [output]'
};
