/*
author: Jelmer Oldersma
version: 0.1
*/
const ytdl = require('ytdl-core-discord');
const ytdl_core = require('ytdl-core');

exports.run = async (client, message, guild, params, perms) => {
    let serverQueue = client.queue.get(guild.id);
    let url = params[0];
    let song = await validate(url)
        .catch(async e => {
            await message.reply('Error:\n' + e);
    });
    if(!song) {
        return;
    } else if(song.age_restricted && perms < 2) {
        await message.reply('You cheeky monkey! That\'s an adult url, and you don\'t have clearance for that Sir!');
        return;
    }

    if (message.member.voiceChannel) {
        let voiceChannel = message.member.voiceChannel;

        if (!serverQueue) {
            const queueConstruct = {
                textChannel: message.channel,
                voiceChannel: voiceChannel,
                connection: null,
                songs: [],
                volume: 5,
                playing: true,
            };

            client.queue.set(message.guild.id, queueConstruct);
            queueConstruct.songs.push(song);

            try {
                queueConstruct.connection = await voiceChannel.join();
                await play(message.guild, queueConstruct.songs[0], client);
            } catch (err) {
                console.log(err);
                client.queue.delete(message.guild.id);
                return message.channel.send(err);
            }
        } else {
            serverQueue.songs.push(song);
            return message.channel.send(`${song.title} has been added to the queue!`);
        }
    }
};

async function play(guild, song, client) {
    const serverQueue = client.queue.get(guild.id);

    if (!song) {
        serverQueue.voiceChannel.leave();
        client.queue.delete(guild.id);
        return;
    }

    // noinspection JSCheckFunctionSignatures
    serverQueue.connection.playOpusStream(await ytdl(song.video_url))
        .on('end', () => {
            console.log('Music ended!');
            serverQueue.songs.shift();
            play(guild, serverQueue.songs[0], client);
        })
        .on('error', error => {
            console.error(error);
        });
}

async function validate(url) {
    if (ytdl_core.validateURL(url)) {
        return ytdl_core.getInfo(url);
    }
    throw 'Could you kindly give me a proper YouTube url Sir?\nI\'m afraid Youtube is still an unfamiliar subject to me, I can\'t provide you with answers based on keywords...';
}
//connection.playOpusStream(await ytdl(url));
exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: [],
    permLevel: 1
};

exports.help = {
    name: 'play',
    description: 'play youtube music',
    usage: 'play [url]'
};
