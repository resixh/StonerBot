/*
author: Jelmer Oldersma
version: 0.1
*/
exports.run = (client, message, guild) => {
    let serverQueue = client.queue.get(guild.id);
    if (!message.member.voiceChannel) return message.channel.send('You have to be in a voice channel to stop the music!');
    if (!serverQueue) return message.channel.send('There is no song that I could skip!');
    serverQueue.connection.dispatcher.end();
};

exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: [],
    permLevel: 1
};

exports.help = {
    name: 'skip',
    description: 'skip current song',
    usage: 'skip'
};
