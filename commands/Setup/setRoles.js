/*
author: Jelmer Oldersma
version: 0.1
*/
const db = require('firebase-admin').database();

exports.run = (client, message, guild, params) => {

    let cmd =  (client.commands.has(params[0]))? //check if command exists
        client.commands.get(params[0]):
        ((client.aliases.has(params[0]))?
            client.commands.get(client.aliases.get(params[0])):
            null);
    //Grab roles grab ids
    let msg_roles = message.mentions.roles;
    let command_name = cmd.help.name;
    let role_ids = [];
    msg_roles.forEach(role => {
        role_ids.push(role.id);
    });

    //set to database, local+firebase

    db.ref('/guilds/'+guild.id+'/command_roles/'+command_name).set(role_ids)
        .then(() => {
            if(!client.localdb.guilds[guild.id].hasOwnProperty('command_roles')) {
                client.localdb.guilds[guild.id].command_roles = {};
            }
            client.localdb.guilds[guild.id].command_roles[command_name] = role_ids;
            console.log(client.localdb.guilds[guild.id].command_roles[command_name]);
        })
        .catch(err => {
            console.log(err);
        });
};

exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: ["setrole", "setRole", "setRoles"],
    permLevel: 2
};

exports.help = {
    name: 'setroles',
    description: 'Set additional Role permissions on commands, only applies to non-mod guildmembers.',
    usage: 'setroles [command] [@role] [optional: more roles through @role tag]'
};
