/*
author: Jelmer Oldersma
version: 0.1
*/

const db = require('firebase-admin').database();

exports.run = (client, message, guild) => {

    let obj = {commandChannel: message.channel.id};
    let user = message.author;

    db.ref('/guilds/'+message.guild.id+'/').update(obj)
        .then(() => {
            client.localdb.guilds[guild.id].commandChannel = message.channel.id;

        })
        .catch(err => {
            console.error(err);
            message.reply('We encountered an error!');

        });


    message.delete();
};

exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: ['setcommandchannel'],
    permLevel: 2
};

exports.help = {
    name: 'setCommandChannel',
    description: 'Sets the command channel, use in desired channel',
    usage: 'setcommandchannel'
};
