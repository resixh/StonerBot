/*
author: Jelmer Oldersma
version: 0.1
*/

const db = require('firebase-admin').database();

exports.run = (client, message, guild, params) => {
    if(!params) {
        message.reply(`Oi give an argument mate!\nProper usage: \`${guild.prefix}setprefix !\` to set it to ! again.`);
        return;
    }

    let obj = {prefix: params[0]};

    db.ref('/guilds/'+guild.id+'/').update(obj)
        .then(() => {
            client.localdb.guilds[guild.id].prefix = obj.prefix;
        })
        .catch(err => {
            console.error(err);
            message.reply('Error error!');
        });

};

exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: ['setprefix'],
    permLevel: 2
};

exports.help = {
    name: 'setPrefix',
    description: 'Sets the prefix the bot uses to recognise commands in chat.',
    usage: 'setprefix [desired prefix]'
};
