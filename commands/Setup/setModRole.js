/*
author: Jelmer Oldersma
version: 0.1
*/

const db = require('firebase-admin').database();
let data = [];

exports.run = async (client, message, guild, params, perms, command) => {
    data = [];

    if(guild.setup === true && perms < 2) {
        await message.reply('Sorry but you arent high enough matey');
        return;
    }

    let obj = {modrole: '', setup: true};
    if(!params[0]) {
        const filter = match => {return match.author === message.author};

        message.channel.send('Please tell me what the mods role is called matey');
        await client.awaitMessage(filter, message.channel, data);
        obj.modrole = data[0];
    } else if(message.mentions.roles.first() != null) {
        obj.modrole = message.mentions.roles.first().name;
    } else {
        let subNum = guild.prefix.length + command.length;
        obj.modrole = message.content.substr(subNum).trim();
    }

    db.ref('/guilds/'+guild.id+'/').update(obj)
        .then(() => {
            client.localdb.guilds[guild.id].modrole = obj.modrole;
            client.localdb.guilds[guild.id].setup = true;
        })
        .catch(err => {
            console.error(err);
            message.reply('Error error!');
        });
};




exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: ['setmodrole'],
    permLevel: 1
};

exports.help = {
    name: 'setModRole',
    description: 'Sets the name of the mods role, used for permissions. After initial setup requires mod permlvl.',
    usage: 'setmodrole [modrole name] OR setmodrole'
};
