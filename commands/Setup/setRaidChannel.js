/*
author: Jelmer Oldersma
version: 0.1
*/
const db = require('firebase-admin').database();

exports.run = (client, message, guild) => {

    let obj = {raidChannel: message.channel.id};
    let user = message.author;

    db.ref('/guilds/'+message.guild.id+'/').update(obj)
        .then(() => {
            client.localdb.guilds[guild.id].raidChannel = message.channel.id;
        })
        .catch(err => {
            console.error(err);
            message.reply('We encountered an error!');
        });

    message.delete();
};

exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: ['setraidchannel'],
    permLevel: 2
};

exports.help = {
    name: 'setRaidChannel',
    description: 'Sets the raid event sign-up channel, use command in desired channel',
    usage: 'setraidchannel'
};
