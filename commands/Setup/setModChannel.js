/*
author: Jelmer Oldersma
version: 0.1
*/

const db = require('firebase-admin').database();

exports.run = (client, message, guild) => {

    let obj = {modChannel: message.channel.id};
    let user = message.author;

    db.ref('/guilds/'+message.guild.id+'/').update(obj)
        .then(() => {
            client.localdb.guilds[guild.id].modChannel = message.channel.id;

        })
        .catch(err => {
            console.error(err);
            message.reply('We encountered an error!');

        });

    message.delete();
};

exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: ['setmodchannel'],
    permLevel: 2
};

exports.help = {
    name: 'setModChannel',
    description: 'Sets the mods channel, use in desired channel',
    usage: 'setmodchannel'
};
