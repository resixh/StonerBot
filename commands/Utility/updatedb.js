/*
author: Jelmer Oldersma
version: 0.1
*/
//const settings = require('../settings.json');

exports.run = async (client) => {
  await client.updateDB();
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: 4
};

exports.help = {
  name: 'updatedb',
  description: 'Resync local-database with Online-database.',
  usage: ''
};
