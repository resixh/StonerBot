/*
author: Jelmer Oldersma
version: 1.0
*/

exports.run = (client, message) => {
  //The best way to test if the bot works, and to see the latency ;)
  message.channel.send('Ping?')
    .then(msg => { //Edit msg with latency and bot version
      msg.edit(`Pong! (took: ${msg.createdTimestamp - message.createdTimestamp}ms)\n`+client.localdb.version);
    });
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: 0
};

exports.help = {
  name: 'ping',
  description: 'Ping/Pong command. I wonder what this does? /sarcasm',
  usage: 'ping'
};
