/*
author: Jelmer Oldersma
version: 0.1
*/
const db = require('firebase-admin').database();

exports.run = (client, message, guild, params) => {

    switch (params[0]) {
        case null:
            noParams();
            break;
        case 'setup':
            setup();
            break;
        case 'new':
            if(params[1] != null) newGuide(params[1]);
            else newGuide(null);
            break;
        case 'delete':
            if (params[1] != null) deleteGuide(params[1]);
            else deleteGuide(null);
            break;
        default:
            if (params[1] != null) getGuide(params[1]);
            else noParams();
            break;
    }
};

exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: ['guides','Guide','Guides'],
    permLevel: 1
};

exports.help = {
    name: 'guide',
    description: 'Show the guides of your guild',
    usage: '!guide [optional params]'
};

//Internal logic :O

// No Parameters = guide overview
function noParams() {

}

// setup param = guide setup overview
function setup() {

}

// new param = new guide logic
function newGuide(name) {

}

// delete param = delete guide logic
function deleteGuide(name) {

}

// param might contain guide name? = check for guide and give it
function getGuide(name) {
    // get 1 or more guides?

    //return multiple results overview?

    //if null? return all guides

}