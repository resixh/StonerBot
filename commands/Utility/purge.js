/*
author: Jelmer Oldersma
version: 1.0
*/

//Clears specified amount of messages
exports.run = function(client, message, prefix, params) {
  let messagecount = parseInt(params.join(' ')) + 1; //Number of messages needed to be removed, +1 for the requesting message.
  message.channel.fetchMessages({
    limit: messagecount
  })
    .then(messages => message.channel.bulkDelete(messages))
    .catch(err => message.channel.send('Couldnt purge messages: ' + err));
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: 3
};

exports.help = {
  name: 'purge',
  description: 'Purges/BulkDelete X amount of messages from a given channel.',
  usage: 'purge <number>'
};
