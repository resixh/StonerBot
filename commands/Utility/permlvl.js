/*
author: Jelmer Oldersma
version: 1.0
*/

exports.run = (client, message, guild, params, perms) => {
  message.reply('Your permlvl is `'+perms+'`.'); //Replies to user with their permlvl
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: ['perms', 'permslvl', 'perm'],
  permLevel: 0
};

exports.help = {
  name: 'permlvl',
  description: 'Shows your permlvl.',
  usage: 'permlvl'
};
