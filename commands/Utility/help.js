/*
author: Jelmer Oldersma
version: 0.9


1. REWORK NEEDED!!!
*/

exports.run = (client, message, guildsettings, params) => {
  if (!params[0]) { //The general help message
    const commandNames = Array.from(client.commands.keys()); //Create array with all command files
    const longest = commandNames.reduce((long, str) => Math.max(long, str.length), 0);
    //sends help msg with all commands available to that user
    message.author.sendCode('asciidoc', `= Command List =\n\n[Use ${guildsettings.prefix}help <commandname> for details]\nYour permlvl: ${client.elevation(message, guildsettings)}\nAll command arguments are written without \`[]\`!\n${client.commands.map(c => {
      //Checks if user is allowed to use the command, returns nothing if he cant
      if(c.conf.permLevel > client.elevation(message, guildsettings)) {
        
      } else {
        return `${guildsettings.prefix}${c.help.name}${' '.repeat(longest - c.help.name.length)} :${c.conf.permLevel}: ${c.help.description}`;
      }
    })
      .join('\n')}`);
  } else {    //Shows help for the specified command
    let command = params[0]; //grabs command
    if (client.commands.has(command)) { //Check if its an actual command
      command = client.commands.get(command);
      message.author.sendCode('asciidoc', `= ${command.help.name} = \n${command.help.description}\nusage::${command.help.usage}`);
    } else if(client.commands.has(client.aliases.get(command))) { //Check if its maybe an alias for a command.
      command = client.commands.get(client.aliases.get(command));
      message.author.sendCode('asciidoc', `= ${command.help.name} = \n${command.help.description}\nusage::${command.help.usage}`);
    } else { //When the command asked is not known
      message.author.send('I dont know that command!?\nUse the help command without any arguments to see all my commands.');
    }
  }
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: ['h', 'halp'],
  permLevel: 0
};

exports.help = {
  name: 'help',
  description: 'Displays all the available commands for your permission level.',
  usage: 'help [command]'
};
