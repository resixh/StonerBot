/*
author: Jelmer Oldersma
version: 0.1
*/
exports.run = async (client, message, guild, params, perms, command) => {

    //get data out of database
    let command_roles;
    command_roles = client.localdb.guilds[guild.id].command_roles;
    let roleData = "**"+ guild.name + "** command-role permissions:\n\n";

    for(let command in command_roles) {
        roleData += "**"+command+"**:\n";
        for (const roleId of command_roles[command]) {
            let role = await message.guild.roles.find(val => val.id === roleId);
            roleData += "- "+role.name+" \n";
        }
        roleData += "\n";
    }

    //send display
    await message.author.send(roleData);

};

exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: ['getroles'],
    permLevel: 1
};

exports.help = {
    name: 'getRoles',
    description: 'Display your guild role-command permissions!',
    usage: 'getroles'
};
